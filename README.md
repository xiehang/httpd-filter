Currently supported Web server:

1. Apache HTTPd 2.2 and 2.4
2. nginx 1.2, 1.4, and 1.5
3. IIS 6 and 7
4. Java Servlet Container (TBD)
