/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* B-Cookie Module
 *
 * This Apache module is designed to track browsers (this is where the "B"
 * comes from), it generated and set a unique browser id as a cookie. If
 * there is a valid B-cookie exists already ("cookie:" header from browser,
 * and validated), it does nothing.
 *
 * Available configuration directives:
 * - BCookieSkip
 *   Description: Skip setting/checking bcookie in certain conditions
 *   Syntax:      BCookieSkip env=[!]environment_variable
 *   Context:     server config, virtual host, directory, .htaccess
 *   Override:    FileInfo
 *   Status:      Extension
 *   Module:      mod_bcookie
 *
 * - BCookieDomain
 *   Description: The domain to which the B cookie applies
 *   Syntax:      BCookieDomain domain
 *   Context:     server config, virtual host, directory, .htaccess
 *   Override:    FileInfo
 *   Status:      Extension
 *   Module:      mod_bcookie
 *
 * - BCookieExpires
 *   Description: Expiry time for the tracking cookie
 *   Syntax:      BCookieExpires expiry-period
 *   Default:     BCookieExpires 10 years
 *   Context:     server config, virtual host, directory, .htaccess
 *   Override:    FileInfo
 *   Status:      Extension
 *   Module:      mod_bcookie
 *
 * - BCookieName
 *   Description: Name of the tracking cookie
 *   Syntax:      BCookieName token
 *   Default:     BCookieName B
 *   Context:     server config, virtual host, directory, .htaccess
 *   Override:    FileInfo
 *   Status:      Extension
 *   Module:      mod_bcookie
 *
 * I copied A LOT of codes from mod_usertrack.c ...
 *
 * Hang Xie, xiehang@snda.com, Nov 19, 2012
 */

#include "apr.h"
#include "apr_lib.h"
#include "apr_strings.h"

#include "apr_md5.h"
#include "apr_base64.h"

#define APR_WANT_STRFUNC
#include "apr_want.h"

#include "httpd.h"
#include "http_config.h"
#include "http_core.h"
#include "http_request.h"
#include "http_log.h"

module AP_MODULE_DECLARE_DATA bcookie_module;

typedef struct {
    enum { SKIP_IF_HAVE, SKIP_IF_NOT_HAVE } criteria;
    char *name;
} skip_cond_rec;
 
typedef struct {
    char *cookie_name;
    char *cookie_domain;
    long cookie_expires;
    char *regexp_string;  /* used to compile regexp; save for debugging */
    ap_regex_t *regexp;   /* used to find bcookie cookie in cookie header */
    apr_array_header_t *cookie_skip_cond;
} cookie_dir_rec;

#define COOKIE_NAME "B"
#define COOKIE_EXPIRES 10*365*24*60*60
#define COOKIE_VERSION 2

/* Make Cookie: generate unique id based on combination of:
 * - time (up to micro second)
 * - server address
 * - server port
 * - client address
 * - client port
 * MD5 then BASE64 then apply our own tricks
*/

static char * compose_bcookie_v1(request_rec *r)
{
    /* compose raw id based on time (up to micro second, and socket pair */
    char * raw_id = apr_psprintf(r->pool, "%f:%s:%u:%s:%u",
                                 apr_time_now()/1000000.0,
                                 r->connection->local_ip, ap_get_server_port(r),
                                 r->connection->remote_ip, r->connection->remote_addr->port);

    /* md5 */
    apr_md5_ctx_t context;
    apr_md5_init(&context);
    apr_size_t nbytes = strlen(raw_id);
    apr_md5_update(&context, raw_id, nbytes);

    unsigned char md5[APR_MD5_DIGESTSIZE];
    apr_md5_final(md5, &context);

    /* base64 */
    char base64[25];
    apr_base64_encode_binary(base64, md5, APR_MD5_DIGESTSIZE);

    /* our own tricks */
    base64[24] = '\0';
    int index;
    for (index=0; index<22; index++) {
        switch (base64[index]) {
            case '/':
                base64[index] = '_';
                break;
            case '+':
                base64[index] = '~';
                break;
            default:
                break;
        }
    }
    base64[22] = base64[ base64[0] % 22 ];
    base64[23] = base64[ base64[22] % 22 ];

    char *cookiebuf;
    cookiebuf = apr_psprintf(r->pool, "v=%u&d=%s", 1, base64);
    ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,
                 "created B-cookie [%s] [%lu] [%s]",
                 raw_id, (long unsigned int)nbytes, cookiebuf);
    return apr_pstrdup(r->pool, cookiebuf);
}

static char * compose_bcookie_v2(request_rec *r)
{
    /* compose raw id based on time (up to micro second, and socket pair */
    char * raw_id = apr_psprintf(r->pool, "%f:%s:%u:%s:%u",
                                 apr_time_now()/1000000.0,
                                 r->connection->local_ip, ap_get_server_port(r),
                                 r->connection->remote_ip, r->connection->remote_addr->port);

    /* md5 */
    apr_md5_ctx_t context;
    apr_md5_init(&context);
    apr_size_t nbytes = strlen(raw_id);
    apr_md5_update(&context, raw_id, nbytes);

    unsigned char md5[APR_MD5_DIGESTSIZE];
    apr_md5_final(md5, &context);

    /* base64 */
    char base64[25];
    apr_base64_encode_binary(base64, md5, APR_MD5_DIGESTSIZE);

    /* our own tricks */
    base64[24] = '\0';
    int index;
    for (index=0; index<22; index++) {
        switch (base64[index]) {
            case '/':
                base64[index] = '_';
                break;
            case '+':
                base64[index] = '.';
                break;
            default:
                break;
        }
    }
    base64[22] = base64[ base64[0] % 22 ];
    base64[23] = base64[ base64[22] % 22 ];

    char *cookiebuf;
    cookiebuf = apr_psprintf(r->pool, "v=%u&d=%s", 2, base64);
    ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,
                 "created B-cookie [%s] [%lu] [%s]",
                 raw_id, (long unsigned int)nbytes, cookiebuf);
    return apr_pstrdup(r->pool, cookiebuf);
}

static void make_cookie(request_rec *r)
{
    char * cookiebuf;
    switch (COOKIE_VERSION) {
        case 1:
            cookiebuf = compose_bcookie_v1(r);
            break;
        case 2:
            cookiebuf = compose_bcookie_v2(r);
            break;
        default:
            break;
    }

    cookie_dir_rec *dcfg = ap_get_module_config(r->per_dir_config, &bcookie_module);

    apr_time_exp_t tms;
    apr_time_exp_gmt(&tms, r->request_time
                         + apr_time_from_sec(dcfg->cookie_expires));

    char *new_cookie = apr_psprintf(r->pool,
                                    "%s=%s; path=/; "
                                    "expires=%s, %.2d-%s-%.2d %.2d:%.2d:%.2d GMT",
                                    dcfg->cookie_name, cookiebuf,
                                    apr_day_snames[tms.tm_wday], tms.tm_mday,
                                    apr_month_snames[tms.tm_mon], tms.tm_year % 100,
                                    tms.tm_hour, tms.tm_min, tms.tm_sec);
    if (dcfg->cookie_domain != NULL) {
        new_cookie = apr_pstrcat(r->pool, new_cookie, "; domain=", dcfg->cookie_domain, NULL);
    }

    apr_table_addn(r->headers_out, "Set-Cookie", new_cookie);
    const char * cookie_header = apr_table_get(r->headers_in, "Cookie");
    if (cookie_header) {
        apr_table_setn(r->headers_in, "Cookie",
                      apr_psprintf(r->pool, "%s; %s=%s", cookie_header, dcfg->cookie_name, cookiebuf));
    } else {
        apr_table_setn(r->headers_in, "Cookie",
                      apr_psprintf(r->pool, "%s=%s", dcfg->cookie_name, cookiebuf));
    }
    apr_table_setn(r->notes, "bcookie", apr_psprintf(r->pool, "%s&new=1", cookiebuf));
    return;
}

/* check if cookie value is valid per v1 definition */
static int is_valid_bcookie_v1(request_rec * r, char * cookieval) {
    if (strlen(cookieval) != 24) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie length");
        return 0;
    }

    if (cookieval[22] != cookieval[ cookieval[0] % 22]) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie byte #22");
        return 0;
    }

    if (cookieval[23] != cookieval[ cookieval[22] % 22]) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie byte #23");
        return 0;
    }

    // need to work on the copy as validation will change the value
    char * data = apr_pstrndup(r->pool, cookieval, 24);
    if (data == NULL) {
        return 0;
    }

    int index;
    for (index=0; index<22; index++) {
        switch (data[index]) {
            case '_':
                data[index] = '/';
                break;
            case '~':
                data[index] = '+';
                break;
            default:
                break;
        }
    }
    data[22] = '=';
    data[23] = '=';

    unsigned char raw[APR_MD5_DIGESTSIZE];
    int length = apr_base64_decode_binary(raw, data);
    if (length != APR_MD5_DIGESTSIZE) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,
                     "invalid B-cookie base64 [%s] [%d]", data, length);
        return 0;
    }

    return 1;
}

/* check if cookie value is valid per v2 definition */
static int is_valid_bcookie_v2(request_rec * r, char * cookieval) {
    if (strlen(cookieval) != 24) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie length");
        return 0;
    }

    if (cookieval[22] != cookieval[ cookieval[0] % 22]) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie byte #22");
        return 0;
    }

    if (cookieval[23] != cookieval[ cookieval[22] % 22]) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,"invalid B-cookie byte #23");
        return 0;
    }

    // need to work on the copy as validation will change the value
    char * data = apr_pstrndup(r->pool, cookieval, 24);
    if (data == NULL) {
        return 0;
    }

    int index;
    for (index=0; index<22; index++) {
        switch (data[index]) {
            case '_':
                data[index] = '/';
                break;
            case '.':
                data[index] = '+';
                break;
            default:
                break;
        }
    }
    data[22] = '=';
    data[23] = '=';

    unsigned char raw[APR_MD5_DIGESTSIZE];
    int length = apr_base64_decode_binary(raw, data);
    if (length != APR_MD5_DIGESTSIZE) {
        ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0, NULL,
                     "invalid B-cookie base64 [%s] [%d]", data, length);
        return 0;
    }

    return 1;
}

/* check if cookie is valid per version definition */
static int is_valid_bcookie(request_rec * r, unsigned int version, char * cookieval) {
    switch (version) {
        case 1:
            if (is_valid_bcookie_v1(r, cookieval)) {
                return 1;
            }
            break;
        case 2:
            if (is_valid_bcookie_v2(r, cookieval)) {
                return 1;
            }
            break;
        default:
            ap_log_error(APLOG_MARK, APLOG_DEBUG|APLOG_NOERRNO, 0,
                         NULL,"invalid B-cookie version");
            break;
    }
    return 0;
}

/* dcfg->regexp is "^cookie_name=v=(\\d+)&d=([^;]+)|;[ \t]+cookie_name=v=(\\d+)&d=([^;]+)",
 * which has five subexpressions, $0..$4 */
#define NUM_SUBS 5

static void set_and_comp_regexp(cookie_dir_rec *dcfg,
                                apr_pool_t *p,
                                const char *cookie_name)
{
    int danger_chars = 0;
    const char *sp = cookie_name;

    /* The goal is to end up with this regexp,
     * ^cookie_name=v=(\\d+)&d=([^;,]+)|[;,][ \t]+cookie_name=v=(\\d+)&d=([^;,]+)
     * with cookie_name obviously substituted either
     * with the real cookie name set by the user in httpd.conf, or with the
     * default COOKIE_NAME. */

    /* Anyway, we need to escape the cookie_name before pasting it
     * into the regex
     */
    while (*sp) {
        if (!apr_isalnum(*sp)) {
            ++danger_chars;
        }
        ++sp;
    }

    if (danger_chars) {
        char *cp;
        cp = apr_palloc(p, sp - cookie_name + danger_chars + 1); /* 1 == \0 */
        sp = cookie_name;
        cookie_name = cp;
        while (*sp) {
            if (!apr_isalnum(*sp)) {
                *cp++ = '\\';
            }
            *cp++ = *sp++;
        }
        *cp = '\0';
    }

    dcfg->regexp_string = apr_pstrcat(p, "^",
                                      cookie_name,
                                      "=v=(\\d+)&d=([^;,]+)|[;,][ \t]*",
                                      cookie_name,
                                      "=v=(\\d+)&d=([^;,]+)", NULL);

    dcfg->regexp = ap_pregcomp(p, dcfg->regexp_string, AP_REG_EXTENDED);
    ap_assert(dcfg->regexp != NULL);
}

static int spot_cookie(request_rec *r)
{
    cookie_dir_rec *dcfg = ap_get_module_config(r->per_dir_config,
                                                &bcookie_module);
    const skip_cond_rec * skip_cond;
    int index;
    const char *var_value;

    /* check if we should skip bcookie checking/setting */
    if (dcfg->cookie_skip_cond) {
        skip_cond = (skip_cond_rec *)dcfg->cookie_skip_cond->elts;
        for (index=0; index<dcfg->cookie_skip_cond->nelts; index++) {
            var_value = apr_table_get(r->subprocess_env, skip_cond[index].name);
            if ((skip_cond[index].criteria == SKIP_IF_HAVE)
                    && (var_value != NULL)) {
                return OK;
            } else if ((skip_cond[index].criteria == SKIP_IF_NOT_HAVE)
                    && (var_value == NULL)) {
                return OK;
            }
        }
    }

    const char *cookie_header;
    ap_regmatch_t regm[NUM_SUBS];

    /* Do not run in subrequests */
    if (r->main) {
        return DECLINED;
    }

    if ((cookie_header = apr_table_get(r->headers_in, "Cookie"))) {
        if (!ap_regexec(dcfg->regexp, cookie_header, NUM_SUBS, regm, 0)) {
            char *cookieval = NULL;
            unsigned int version = 0;
            /* Our regexp,
             * ^cookie_name=v=(\d+)&d=([^;]+)|;[ \t]+cookie_name=v=(\d+)&d=([^;]+)
             * only allows for $1 to $4 to be available. ($0 is always
             * filled with the entire matched expression, not just
             * the part in parentheses.) So just check for either one
             * and assign to cookieval if present. */
            if ((regm[1].rm_so != -1) && (regm[2].rm_so != -1)) {
                version = atoi(ap_pregsub(r->pool, "$1", cookie_header,
                                          NUM_SUBS, regm));
                cookieval = ap_pregsub(r->pool, "$2", cookie_header,
                                       NUM_SUBS, regm);
            }
            if ((regm[3].rm_so != -1) && (regm[4].rm_so != -1)) {
                version = atoi(ap_pregsub(r->pool, "$3", cookie_header,
                                          NUM_SUBS, regm));
                cookieval = ap_pregsub(r->pool, "$4", cookie_header,
                                       NUM_SUBS, regm);
            }

            /* verify cookie */
            if (is_valid_bcookie(r, version, cookieval)) {
                apr_table_setn(r->notes, "bcookie",
                               apr_psprintf(r->pool, "v=%u&d=%s",
                                            version, cookieval));
                return OK;
            }
        }
    }
    make_cookie(r);
    return OK;                  /* We set our cookie */
}

static void *make_cookie_dir(apr_pool_t *p, char *d)
{
    cookie_dir_rec *dcfg;

    dcfg = (cookie_dir_rec *) apr_pcalloc(p, sizeof(cookie_dir_rec));
    dcfg->cookie_name = COOKIE_NAME;
    dcfg->cookie_domain = NULL;
    dcfg->cookie_expires = COOKIE_EXPIRES;
    dcfg->cookie_skip_cond = NULL;

    /* In case the user does not use the CookieName directive,
     * we need to compile the regexp for the default cookie name. */
    set_and_comp_regexp(dcfg, p, COOKIE_NAME);

    return dcfg;
}

static const char *set_cookie_skip_cond(cmd_parms *parms, void *mconfig,
                                      const char *arg)
{
    cookie_dir_rec *dcfg = (cookie_dir_rec *)mconfig;
    if (dcfg->cookie_skip_cond == NULL) {
        dcfg->cookie_skip_cond = apr_array_make(parms->pool, 1,
                                                sizeof(skip_cond_rec));
    }
    skip_cond_rec * new_cond = apr_array_push(dcfg->cookie_skip_cond);
    if (!strncasecmp(arg, "env=!", 5)) {
        new_cond->criteria = SKIP_IF_NOT_HAVE;
        new_cond->name = apr_pstrdup(parms->pool, arg+5);
    } else if (!strncasecmp(arg, "env=", 4)) {
        new_cond->criteria = SKIP_IF_HAVE;
        new_cond->name = apr_pstrdup(parms->pool, arg+4);
    } else {
        return "BCookieSkip should start with env= or env=!";
    }

    return NULL;
}

static const char *set_cookie_exp(cmd_parms *parms, void *mconfig,
                                  const char *arg)
{
    cookie_dir_rec *dcfg = (cookie_dir_rec *) mconfig;
    time_t factor, modifier = 0;
    time_t num = 0;
    char *word;

    /* The simple case first - all numbers (we assume) */
    if (apr_isdigit(arg[0]) && apr_isdigit(arg[strlen(arg) - 1])) {
        dcfg->cookie_expires = atol(arg);
        return NULL;
    }

    /*
     * The harder case - stolen from mod_expires
     *
     * CookieExpires "[plus] {<num> <type>}*"
     */

    word = ap_getword_conf(parms->pool, &arg);
    if (!strncasecmp(word, "plus", 1)) {
        word = ap_getword_conf(parms->pool, &arg);
    };

    /* {<num> <type>}* */
    while (word[0]) {
        /* <num> */
        if (apr_isdigit(word[0]))
            num = atoi(word);
        else
            return "bad expires code, numeric value expected.";

        /* <type> */
        word = ap_getword_conf(parms->pool, &arg);
        if (!word[0])
            return "bad expires code, missing <type>";

        factor = 0;
        if (!strncasecmp(word, "years", 1))
            factor = 60 * 60 * 24 * 365;
        else if (!strncasecmp(word, "months", 2))
            factor = 60 * 60 * 24 * 30;
        else if (!strncasecmp(word, "weeks", 1))
            factor = 60 * 60 * 24 * 7;
        else if (!strncasecmp(word, "days", 1))
            factor = 60 * 60 * 24;
        else if (!strncasecmp(word, "hours", 1))
            factor = 60 * 60;
        else if (!strncasecmp(word, "minutes", 2))
            factor = 60;
        else if (!strncasecmp(word, "seconds", 1))
            factor = 1;
        else
            return "bad expires code, unrecognized type";

        modifier = modifier + factor * num;

        /* next <num> */
        word = ap_getword_conf(parms->pool, &arg);
    }

    dcfg->cookie_expires = modifier;

    return NULL;
}

static const char *set_cookie_name(cmd_parms *cmd, void *mconfig,
                                   const char *name)
{
    cookie_dir_rec *dcfg = (cookie_dir_rec *) mconfig;

    dcfg->cookie_name = apr_pstrdup(cmd->pool, name);

    set_and_comp_regexp(dcfg, cmd->pool, name);

    if (dcfg->regexp == NULL) {
        return "Regular expression could not be compiled.";
    }
    if (dcfg->regexp->re_nsub + 1 != NUM_SUBS) {
        return apr_pstrcat(cmd->pool, "Invalid cookie name \"",
                           name, "\"", NULL);
    }

    return NULL;
}

/*
 * Set the value for the 'Domain=' attribute.
 */
static const char *set_cookie_domain(cmd_parms *cmd, void *mconfig,
                                     const char *name)
{
    cookie_dir_rec *dcfg;

    dcfg = (cookie_dir_rec *) mconfig;

    /*
     * Apply the restrictions on cookie domain attributes.
     */
    if (strlen(name) == 0) {
        return "BCookieDomain values may not be null";
    }
    if (name[0] != '.') {
        return "BCookieDomain values must begin with a dot";
    }
    if (ap_strchr_c(&name[1], '.') == NULL) {
        return "BCookieDomain values must contain at least one embedded dot";
    }

    dcfg->cookie_domain = apr_pstrdup(cmd->pool, name);
    return NULL;
}

static const command_rec bcookie_cmd_table[] = {
    AP_INIT_TAKE1("BCookieSkip", set_cookie_skip_cond, NULL, OR_FILEINFO,
                  "condition to skip bcookie"),
    AP_INIT_TAKE1("BCookieExpires", set_cookie_exp, NULL, OR_FILEINFO,
                  "an expiry date code"),
    AP_INIT_TAKE1("BCookieDomain", set_cookie_domain, NULL, OR_FILEINFO,
                  "domain to which B cookie applies"),
    AP_INIT_TAKE1("BCookieName", set_cookie_name, NULL, OR_FILEINFO,
                  "name of the B cookie"),
    {NULL}
};

static void register_hooks(apr_pool_t *p)
{
    ap_hook_fixups(spot_cookie,NULL,NULL,APR_HOOK_FIRST);
}

module AP_MODULE_DECLARE_DATA bcookie_module = {
    STANDARD20_MODULE_STUFF,
    make_cookie_dir,            /* dir config creater */
    NULL,                       /* dir merger --- default is to override */
    NULL,                       /* server config */
    NULL,                       /* merge server configs */
    bcookie_cmd_table,          /* command apr_table_t */
    register_hooks              /* register hooks */
};
