/*
 * Copyright (C) Shanda Innovations
 */


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>
#include <ngx_md5.h>

#define CURRENT_VERSION 2
#define INVALID_VERSION 0

/* 31 Dec 2037 23:55:55 GMT */
#define NGX_HTTP_BCOOKIE_MAX_EXPIRES  2145916555
/* in string format */
static u_char expires_template[] = "; expires=Thu, 31-Dec-37 23:55:55 GMT";

#define BCOOKIE_PATH_DATA "; path=/"
#define BCOOKIE_PATH_SIZE 8

typedef struct {
    ngx_str_t   name;
    ngx_str_t   domain;
    time_t      expires;
    ngx_array_t *skip_include;
    ngx_array_t	*skip_exclude;
} ngx_http_bcookie_conf_t;


typedef struct {
    ngx_str_t   cookie;
    ngx_str_t   uuid;
    ngx_str_t   data;
    ngx_uint_t  version;
    ngx_uint_t  created;
} ngx_http_bcookie_ctx_t;


/* get bcookie from request */
static ngx_int_t ngx_http_bcookie_populate(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
static ngx_int_t ngx_http_bcookie_get(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
/* set bcookie to response and current request */
static ngx_int_t ngx_http_bcookie_set(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
/* set variable ("bcookie") for other facilities */
static ngx_int_t ngx_http_bcookie_set_logging_variable(ngx_http_request_t *r,
    ngx_http_variable_value_t *v, uintptr_t data);
static ngx_int_t ngx_http_bcookie_set_upstream_variable(ngx_http_request_t *r,
    ngx_http_variable_value_t *v, uintptr_t data);

/* create new bcookie */
static ngx_int_t ngx_http_bcookie_create(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
/* create new v1 bcookie */
static ngx_int_t ngx_http_bcookie_create_v1(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
/* create new v2 bcookie */
static ngx_int_t ngx_http_bcookie_create_v2(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf);
/* generate UUID for bcookie based on connection and time */
static ngx_int_t ngx_http_bcookie_generate_uuid(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx);

/* validate if data is valid in V1 format */
static ngx_int_t ngx_http_bcookie_validate_v1(ngx_http_request_t *r,
    ngx_str_t *data);
/* validate if data is valid in V2 format */
static ngx_int_t ngx_http_bcookie_validate_v2(ngx_http_request_t *r,
    ngx_str_t *data);

/* called before anything */
static void *ngx_http_bcookie_create_conf(ngx_conf_t *cf);
/* called before anything else */
static ngx_int_t ngx_http_bcookie_add_variable(ngx_conf_t *conf);
/* initialize everything after configuration is parsed */
static ngx_int_t ngx_http_bcookie_init(ngx_conf_t *cf);
/* merge configuration with up-level */
static char *ngx_http_bcookie_merge_conf(ngx_conf_t *cf, void *parent,
    void *child);

/* deal with bcookie_domain directive */
static char *ngx_http_bcookie_domain(ngx_conf_t *cf, void *post, void *data);
/* deal with bcookie_expires directive */
static char *ngx_http_bcookie_expires(ngx_conf_t *cf, ngx_command_t *cmd,
    void *conf);
/* deal with bcookie_skip directives */
static char *ngx_http_bcookie_skip(ngx_conf_t *cf, ngx_command_t *cmd,
    void *conf);


static ngx_http_output_header_filter_pt  ngx_http_next_header_filter;


static ngx_conf_post_handler_pt  ngx_http_bcookie_domain_p =
    ngx_http_bcookie_domain;


static ngx_command_t  ngx_http_bcookie_commands[] = {

    { ngx_string("bcookie_name"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_bcookie_conf_t, name),
      NULL },

    { ngx_string("bcookie_domain"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_HTTP_LOC_CONF_OFFSET,
      offsetof(ngx_http_bcookie_conf_t, domain),
      &ngx_http_bcookie_domain_p },

    { ngx_string("bcookie_expires"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_http_bcookie_expires,
      NGX_HTTP_LOC_CONF_OFFSET,
      0,
      NULL },

    { ngx_string("bcookie_skip"),
      NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_LOC_CONF|NGX_CONF_TAKE1,
      ngx_http_bcookie_skip,
      NGX_HTTP_LOC_CONF_OFFSET,
      0,
      NULL },

      ngx_null_command
};


static ngx_http_module_t  ngx_http_bcookie_filter_module_ctx = {
    ngx_http_bcookie_add_variable,         /* preconfiguration */
    ngx_http_bcookie_init,                 /* postconfiguration */

    NULL,                                  /* create main configuration */
    NULL,                                  /* init main configuration */

    NULL,                                  /* create server configuration */
    NULL,                                  /* merge server configuration */

    ngx_http_bcookie_create_conf,          /* create location configuration */
    ngx_http_bcookie_merge_conf            /* merge location configuration */
};


ngx_module_t  ngx_http_bcookie_filter_module = {
    NGX_MODULE_V1,
    &ngx_http_bcookie_filter_module_ctx,    /* module context */
    ngx_http_bcookie_commands,              /* module directives */
    NGX_HTTP_MODULE,                        /* module type */
    NULL,                                   /* init master */
    NULL,                                   /* init module */
    NULL,                                   /* init process */
    NULL,                                   /* init thread */
    NULL,                                   /* exit thread */
    NULL,                                   /* exit process */
    NULL,                                   /* exit master */
    NGX_MODULE_V1_PADDING
};


/* seperate variables for logging and upstream to make logic clear */
static ngx_str_t ngx_http_bcookie_logging_variable
    = ngx_string("bcookie_logging");
static ngx_str_t ngx_http_bcookie_upstream_variable
    = ngx_string("bcookie_upstream");


static ngx_int_t
ngx_http_bcookie_filter(ngx_http_request_t *r)
{
    ngx_http_bcookie_ctx_t   *ctx;
    ngx_http_bcookie_conf_t  *conf;

    if (r != r->main) {
        return ngx_http_next_header_filter(r);
    }

    /* get configuration */
    conf = ngx_http_get_module_loc_conf(r, ngx_http_bcookie_filter_module);
    if (conf == NULL) {
        return NGX_ERROR;
    }

    /* get context */
    ctx = ngx_http_get_module_ctx(r, ngx_http_bcookie_filter_module);
    if (ctx == NULL) {
        ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_bcookie_filter_module));
        if (ctx == NULL) {
            return NGX_ERROR;
        }

        ngx_http_set_ctx(r, ctx, ngx_http_bcookie_filter_module);
    }

    /* populate bcookie only once, either get it from request or create */
    if (ctx->cookie.len == 0) {
        if (ngx_http_bcookie_populate(r, ctx, conf) != NGX_OK) {
            return NGX_ERROR;
        }
    }

    return ngx_http_next_header_filter(r);
}

static ngx_int_t
ngx_http_bcookie_populate(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf)
{
    ctx->created = 0;
    /* check if we have valid bcookie in request */
    if (ngx_http_bcookie_get(r, ctx, conf) != NGX_OK) {
        return NGX_ERROR;
    }

    if (ctx->version == INVALID_VERSION) {
        /* generate new cookie in case cookie we got is invalid */
        ctx->created = 1;
        if (ngx_http_bcookie_set(r, ctx, conf) != NGX_OK) {
            return NGX_ERROR;
        }
        if (ctx->version != CURRENT_VERSION) {
            /* this should never happen */
            ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                          "new bcookie should have version [%D] but got [%V]",
                          CURRENT_VERSION, ctx->cookie);
            return NGX_ERROR;
        }
    }
    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_set_logging_variable(ngx_http_request_t *r,
    ngx_http_variable_value_t *v, uintptr_t data)
{
    ngx_http_bcookie_ctx_t *ctx;
    ngx_http_bcookie_conf_t *conf;

    v->not_found = 1;

    /* get configuration and decide to go on or not */
    conf = ngx_http_get_module_loc_conf(r->main, ngx_http_bcookie_filter_module);
    if (conf == NULL) {
        return NGX_OK;
    }

    ctx = ngx_http_get_module_ctx(r, ngx_http_bcookie_filter_module);
    if (ctx == NULL) {
        ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_bcookie_filter_module));
        if (ctx == NULL) {
            return NGX_ERROR;
        }

        ngx_http_set_ctx(r, ctx, ngx_http_bcookie_filter_module);
    }

    /* populate bcookie only once, either get it from request or create */
    if (ctx->cookie.len == 0) {
        if (ngx_http_bcookie_populate(r, ctx, conf) != NGX_OK) {
            return NGX_ERROR;
        }
    }

    /* set the value of the variable */
    if (ctx->created) {
        /* format of logging newly create cookie is cookiedata&new=1 */
        v->len = ctx->cookie.len + 6;
        v->data = ngx_pnalloc(r->pool, v->len);
        if (v->data == NULL) {
            return NGX_ERROR;
        }
        ngx_sprintf(v->data, "%V&new=1", &ctx->cookie);
    } else {
        v->len = ctx->cookie.len;
        v->data = ngx_pstrdup(r->pool, &ctx->cookie);
        if (v->data == NULL) {
            return NGX_ERROR;
        }
    }

    v->valid = 1;
    v->no_cacheable = 0;
    v->not_found = 0;
    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_set_upstream_variable(ngx_http_request_t *r,
    ngx_http_variable_value_t *v, uintptr_t data)
{
    ngx_http_bcookie_ctx_t *ctx;
    ngx_http_bcookie_conf_t *conf;

    v->not_found = 1;

    /* get configuration and decide to go on or not */
    conf = ngx_http_get_module_loc_conf(r->main, ngx_http_bcookie_filter_module);
    if (conf == NULL) {
        return NGX_OK;
    }

    ctx = ngx_http_get_module_ctx(r, ngx_http_bcookie_filter_module);
    if (ctx == NULL) {
        ctx = ngx_pcalloc(r->pool, sizeof(ngx_http_bcookie_filter_module));
        if (ctx == NULL) {
            return NGX_ERROR;
        }

        ngx_http_set_ctx(r, ctx, ngx_http_bcookie_filter_module);
    }

    /* populate bcookie only once, either get it from request or create */
    if (ctx->cookie.len == 0) {
        if (ngx_http_bcookie_populate(r, ctx, conf) != NGX_OK) {
            return NGX_ERROR;
        }
    }

    /* get existing cookie header */
    ngx_str_t cookie_header;
    if (r->headers_in.cookies.nelts == 0) {
        cookie_header.len = 0;
    } else {
        ngx_table_elt_t *raw_cookie =
            *((ngx_table_elt_t **)r->headers_in.cookies.elts);
        cookie_header.len = raw_cookie->value.len;
        cookie_header.data = raw_cookie->value.data;
    }

    /* set the value of the variable */
    if (ctx->created) {
        /* bcookie was newly created, we need to append it to cookie header */
        v->len = cookie_header.len + 2 + conf->name.len + 1 + ctx->cookie.len;
        v->data = ngx_palloc(r->pool, v->len);
        if (v->data == NULL) {
            return NGX_ERROR;
        }
        if (cookie_header.len != 0) {
            ngx_sprintf(v->data,
                        "%V; %V=%V",
                        &cookie_header, &conf->name, &ctx->cookie);
        } else {
            v->len = ngx_sprintf(v->data,
                                 "%V=%V",
                                 &conf->name, &ctx->cookie) - v->data;
        }
    } else {
        v->len = cookie_header.len;
        v->data = ngx_pstrdup(r->pool, &cookie_header);
        if (v->data == NULL) {
            return NGX_ERROR;
        }
    }

    v->valid = 1;
    v->no_cacheable = 0;
    v->not_found = 0;
    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_get(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf)
{
    /* by default we believe it is a invalid bcookie */
    ctx->version = INVALID_VERSION;

    /* try to get bcookie from request header */
    ngx_int_t n = ngx_http_parse_multi_header_lines(&r->headers_in.cookies,
                                                    &conf->name,
                                                    &ctx->cookie);
    if (n == NGX_DECLINED) {
        /* no bcookie found */
        return NGX_OK;
    }

    /* validate bcookie */
    if ((ctx->cookie.data[0] != 'v') || (ctx->cookie.data[1] != '=')) {
        return NGX_OK;
    }

    u_char *delimiter = (u_char *)ngx_strchr(ctx->cookie.data, '&');
    if (delimiter == NULL) {
        return NGX_OK;
    }

    if ((*(delimiter+1) != 'd') || (*(delimiter+2) != '=')) {
        return NGX_OK;
    }

    /* get version */
    u_char *version_start = ctx->cookie.data+2;
    ngx_int_t version = ngx_atoi(version_start, delimiter-version_start);
    if (version == NGX_ERROR) {
        return NGX_OK;
    }

    ngx_str_t data;
    data.len = ctx->cookie.len - (delimiter+3 - ctx->cookie.data);
    data.data = delimiter+3;

    switch (version) {
        case 1:
            if (ngx_http_bcookie_validate_v1(r, &data) != NGX_OK) {
                return NGX_OK;
            }
            break;
        case 2:
            if (ngx_http_bcookie_validate_v2(r, &data) != NGX_OK) {
                return NGX_OK;
            }
            break;
        default:
            return NGX_OK;
    }

    /* finally everything's good */
    ctx->version = version;
    ctx->data.len = 24;
    ctx->data.data = ngx_pnalloc(r->pool, ctx->data.len);
    if (ctx->data.data == NULL) {
        return NGX_ERROR;
    }
    ngx_memcpy(ctx->data.data, delimiter+3,ctx->data.len);

    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_set(ngx_http_request_t *r,
    ngx_http_bcookie_ctx_t *ctx, ngx_http_bcookie_conf_t *conf)
{
    if (ngx_http_bcookie_create(r, ctx, conf) != NGX_OK) {
        return NGX_ERROR;
    }
    if (ctx->version != CURRENT_VERSION) {
        return NGX_ERROR;
    }

    /* check variables to see if we need to handle bcookie or not */
    ngx_uint_t n;
    ngx_int_t key;
    ngx_str_t *var_name;

    /* if any inclusive variable defined, we will skip */
    if (conf->skip_include) {
        var_name = conf->skip_include->elts;
        for (n=0; n<conf->skip_include->nelts; n++) {
            key = ngx_hash_strlow(var_name[n].data,
                                  var_name[n].data, var_name[n].len);
            if (ngx_http_get_variable(r, var_name, key) != NULL) {
                return NGX_OK;
            }
        }
    }

    /* if any exclusive variable not defined, we will skip */
    if (conf->skip_exclude) {
        var_name = conf->skip_exclude->elts;
        for (n=0; n<conf->skip_exclude->nelts; n++) {
            key = ngx_hash_strlow(var_name[n].data,
                                  var_name[n].data, var_name[n].len);
            if (ngx_http_get_variable(r, var_name, key) == NULL) {
                return NGX_OK;
            }
        }
    }

    /* create a Set-Cookie header for outgoing response */
    ngx_table_elt_t *set_cookie = ngx_list_push(&r->headers_out.headers);
    if (set_cookie == NULL) {
        return NGX_ERROR;
    }

    /* cookie header */
    set_cookie->hash = 1;
    ngx_str_set(&set_cookie->key, "Set-Cookie");

    /* convert expires to string */
    ngx_str_t expires;

    /* alloc memory, -1 to get rid of trailing \0 */
    expires.len = sizeof(expires_template) - 1;
    expires.data = ngx_pnalloc(r->pool, expires.len);
    if (expires.data == NULL) {
        return NGX_ERROR;
    }
    u_char * p = expires.data;

    /* copy data to expires, -1 to get rid of trailing \0 */
    p = ngx_cpymem(p, expires_template, sizeof("; expires=") - 1);
    p = ngx_http_cookie_time(p, ngx_time() + conf->expires);

    /* name=value, then expires, domain, and path */
    set_cookie->value.len = conf->name.len + 1 + ctx->cookie.len
                            + expires.len + conf->domain.len
                            + ngx_strlen(BCOOKIE_PATH_DATA);
    set_cookie->value.data = ngx_pnalloc(r->pool, set_cookie->value.len);
    if (set_cookie->value.data == NULL) {
        return NGX_ERROR;
    }
    ngx_sprintf(set_cookie->value.data,
                "%V=%V%V%V%s",
                &conf->name, &ctx->cookie,
                &expires, &conf->domain, BCOOKIE_PATH_DATA);
    ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                  "generated and sent new bcookie [%V]", &set_cookie->value);

    return NGX_OK;
}


static void *
ngx_http_bcookie_create_conf(ngx_conf_t *cf)
{
    ngx_http_bcookie_conf_t  *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_http_bcookie_conf_t));
    if (conf == NULL) {
        return NULL;
    }

    /*
     * set by ngx_pcalloc():
     *
     *     conf->name = { 0, NULL };
     *     conf->domain = { 0, NULL };
     */

    conf->expires = NGX_CONF_UNSET;

    return conf;
}


static char *
ngx_http_bcookie_merge_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_http_bcookie_conf_t *prev = parent;
    ngx_http_bcookie_conf_t *conf = child;

    ngx_conf_merge_str_value(conf->name, prev->name, "B");
    ngx_conf_merge_str_value(conf->domain, prev->domain, "");
    ngx_conf_merge_sec_value(conf->expires, prev->expires, 0);
    ngx_conf_merge_ptr_value(conf->skip_include, prev->skip_include, NULL);
    ngx_conf_merge_ptr_value(conf->skip_exclude, prev->skip_exclude, NULL);

    return NGX_CONF_OK;
}


static ngx_int_t
ngx_http_bcookie_add_variable(ngx_conf_t *conf)
{
    /* add the variable for logging */
    ngx_http_variable_t *var_logging = ngx_http_add_variable(conf,
        &ngx_http_bcookie_logging_variable, 0);
    if (var_logging == NULL) {
        return NGX_ERROR;
    }
    var_logging->get_handler = ngx_http_bcookie_set_logging_variable;

    /* add the variable for pass to upstream */
    ngx_http_variable_t *var_upstream = ngx_http_add_variable(conf,
        &ngx_http_bcookie_upstream_variable, 0);
    if (var_upstream == NULL) {
        return NGX_ERROR;
    }
    var_upstream->get_handler = ngx_http_bcookie_set_upstream_variable;

    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_init(ngx_conf_t *conf)
{
    /* insert our handler to the chain */
    ngx_http_next_header_filter = ngx_http_top_header_filter;
    ngx_http_top_header_filter = ngx_http_bcookie_filter;

    return NGX_OK;
}


static char *
ngx_http_bcookie_domain(ngx_conf_t *cf, void *post, void *data)
{
    ngx_str_t  *domain = data;

    u_char  *p, *new;

    if (ngx_strcmp(domain->data, "none") == 0) {
        ngx_str_set(domain, "");
        return NGX_CONF_OK;
    }

    new = ngx_pnalloc(cf->pool, sizeof("; domain=") - 1 + domain->len);
    if (new == NULL) {
        return NGX_CONF_ERROR;
    }

    p = ngx_cpymem(new, "; domain=", sizeof("; domain=") - 1);
    ngx_memcpy(p, domain->data, domain->len);

    domain->len += sizeof("; domain=") - 1;
    domain->data = new;

    return NGX_CONF_OK;
}


static char *
ngx_http_bcookie_expires(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_bcookie_conf_t *ucf = conf;

    ngx_str_t  *value;

    if (ucf->expires != NGX_CONF_UNSET) {
        return "is duplicate";
    }

    value = cf->args->elts;

    /* use default longest expires, i.e. 10 years */
    if (ngx_strcmp(value[1].data, "max") == 0) {
        ucf->expires = NGX_HTTP_BCOOKIE_MAX_EXPIRES;
        return NGX_CONF_OK;
    }

    ucf->expires = ngx_parse_time(&value[1], 1);
    if (ucf->expires == (time_t) NGX_ERROR) {
        return "invalid value";
    }

    return NGX_CONF_OK;
}


static char *
ngx_http_bcookie_skip(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ngx_http_bcookie_conf_t *ucf = conf;

    ngx_str_t  *value;

    if (!ucf->skip_include
        && (ngx_array_init(ucf->skip_include, cf->pool, 1, sizeof(ngx_str_t))
            != NGX_OK)
        && (ngx_array_init(ucf->skip_exclude, cf->pool, 1, sizeof(ngx_str_t))
            != NGX_OK)) {
        return "failed to init bcookie_skip";
    }

    value = cf->args->elts;
    ngx_uint_t n;
    ngx_str_t  *var_name;
    for (n = 1; n < cf->args->nelts; n++) {
        if (value[n].data[0] == '!') {
            var_name = ngx_array_push(ucf->skip_exclude);
            if (var_name) {
                var_name->data = &value[n].data[1];
                var_name->len = value[n].len - 1;
            } else {
                return "failed to keep exclude skip";
            }
        } else {
            var_name = ngx_array_push(ucf->skip_include);
            if (var_name) {
                var_name->data = value[n].data;
                var_name->len = value[n].len;
            } else {
                return "failed to keep include skip";
            }
        }
    }

    return NGX_CONF_OK;
}


static ngx_int_t
ngx_http_bcookie_generate_uuid(ngx_http_request_t *r, ngx_http_bcookie_ctx_t *ctx)
{
    if (ctx == NULL) {
        return NGX_ERROR;
    }

    /*
     * max length of the uuid will be:
     * second . micro_second : sever_ip : server_port : client_ip : client_port
     *   11   1      6       1    X     1     5       1    X      1      5
     * where X is NGX_SOCKADDR_STRLEN
     */
    ctx->uuid.len = 2*NGX_SOCKADDR_STRLEN + 32;
    ctx->uuid.data = ngx_pnalloc(r->pool, ctx->uuid.len);
    if (ctx->uuid.data == NULL) {
        return NGX_ERROR;
    }

    /* get server's IP address as a string */
    ngx_str_t server_ip;
    u_char server_addr[NGX_SOCKADDR_STRLEN];

    server_ip.len = NGX_SOCKADDR_STRLEN;
    server_ip.data = server_addr;

    if (ngx_connection_local_sockaddr(r->connection, &server_ip, 0) != NGX_OK) {
        return NGX_ERROR;
    }

    /* get server's port */
    ngx_uint_t server_port;
    struct sockaddr_in *sin;
#if (NGX_HAVE_INET6)
    struct sockaddr_in6 *sin6;
#endif

    switch (r->connection->local_sockaddr->sa_family) {
#if (NGX_HAVE_INET6)
        case AF_INET6:
            sin6 = (struct sockaddr_in6 *) r->connection->local_sockaddr;
            server_port = sin6->sin6_port;
            break;
#endif
        default: /* AF_INET */
            sin = (struct sockaddr_in *) r->connection->local_sockaddr;
            server_port = sin->sin_port;
            break;
    }

    /* get client's IP address as a string */
    ngx_str_t client_ip;
    client_ip.len = r->connection->addr_text.len;
    client_ip.data = r->connection->addr_text.data;

    /* get client's port */
    ngx_uint_t client_port;

    switch (r->connection->sockaddr->sa_family) {
#if (NGX_HAVE_INET6)
        case AF_INET6:
            sin6 = (struct sockaddr_in6 *) r->connection->sockaddr;
            client_port = sin6->sin6_port;
            break;
#endif
        default: /* AF_INET */
            sin = (struct sockaddr_in *) r->connection->sockaddr;
            client_port = sin->sin_port;
            break;
    }

    /* get current time down to micro second */
    struct timeval tp;
    ngx_gettimeofday(&tp);

    /* let's compose the UUID */
    ctx->uuid.len = ngx_sprintf(ctx->uuid.data,
                                "%d.%06d:%V:%d:%V:%d",
                                tp.tv_sec, tp.tv_usec,
                                &server_ip, server_port,
                                &client_ip, client_port) - ctx->uuid.data;
    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_create_v1(ngx_http_request_t *r, ngx_http_bcookie_ctx_t *ctx,
    ngx_http_bcookie_conf_t *conf)
{
    if (ngx_http_bcookie_generate_uuid(r, ctx) != NGX_OK) {
        return NGX_ERROR;
    }

    /* get MD5 of UUID */
    ngx_md5_t md5;
    u_char md5_buf[16];
    ngx_str_t md5_str;
    ngx_md5_init(&md5);
    ngx_md5_update(&md5, ctx->uuid.data, ctx->uuid.len);
    ngx_md5_final(md5_buf, &md5);
    md5_str.len = 16;
    md5_str.data = md5_buf;

    /* base64 it */
    u_char base64_buf[24];
    ngx_str_t base64_str;
    base64_str.len = 24;
    base64_str.data = base64_buf;
    ngx_encode_base64(&base64_str, &md5_str);


    /* do our own trick */
    int index;
    for (index=0; index<22; index++) {
        switch (base64_buf[index]) {
            case '/':
                base64_buf[index] = '_';
                break;
            case '+':
                base64_buf[index] = '~';
                break;
            default:
                break;
        }
    }
    base64_buf[22] = base64_buf[ base64_buf[0] % 22 ];
    base64_buf[23] = base64_buf[ base64_buf[22] % 22 ];

    /* populate the cookie */
    ctx->version = 1;
    ctx->cookie.len = ngx_sprintf(ctx->cookie.data,
                                  "v=%d&d=%V",
                                  ctx->version, &base64_str) - ctx->cookie.data;
    ngx_memcpy(ctx->data.data, base64_str.data, base64_str.len);

    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_create_v2(ngx_http_request_t *r, ngx_http_bcookie_ctx_t *ctx,
    ngx_http_bcookie_conf_t *conf)
{
    if (ngx_http_bcookie_generate_uuid(r, ctx) != NGX_OK) {
        return NGX_ERROR;
    }

    /* get MD5 of UUID */
    ngx_md5_t md5;
    u_char md5_buf[16];
    ngx_str_t md5_str;
    ngx_md5_init(&md5);
    ngx_md5_update(&md5, ctx->uuid.data, ctx->uuid.len);
    ngx_md5_final(md5_buf, &md5);
    md5_str.len = 16;
    md5_str.data = md5_buf;

    /* base64 it */
    u_char base64_buf[24];
    ngx_str_t base64_str;
    base64_str.len = 24;
    base64_str.data = base64_buf;
    ngx_encode_base64(&base64_str, &md5_str);


    /* do our own trick */
    int index;
    for (index=0; index<22; index++) {
        switch (base64_buf[index]) {
            case '/':
                base64_buf[index] = '_';
                break;
            case '+':
                base64_buf[index] = '.';
                break;
            default:
                break;
        }
    }
    base64_buf[22] = base64_buf[ base64_buf[0] % 22 ];
    base64_buf[23] = base64_buf[ base64_buf[22] % 22 ];

    /* populate the cookie */
    ctx->version = 2;
    ctx->cookie.len = ngx_sprintf(ctx->cookie.data,
                                  "v=%d&d=%V",
                                  ctx->version, &base64_str) - ctx->cookie.data;
    ngx_memcpy(ctx->data.data, base64_str.data, base64_str.len);

    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_create(ngx_http_request_t *r, ngx_http_bcookie_ctx_t *ctx,
    ngx_http_bcookie_conf_t *conf)
{
    /*
     * cookie format: v=V&d=DATA
     * where V could be 1 to 999, and DATA should be exact 24 bytes
     */
    ctx->cookie.len = 32;
    ctx->cookie.data = ngx_pnalloc(r->pool, ctx->cookie.len);
    if (ctx->cookie.data == NULL) {
        return NGX_ERROR;
    }

    ctx->data.len = 24;
    ctx->data.data = ngx_pnalloc(r->pool, ctx->data.len);
    if (ctx->data.data == NULL) {
        return NGX_ERROR;
    }

    switch (CURRENT_VERSION) {
        case 1:
            return ngx_http_bcookie_create_v1(r, ctx, conf);
        case 2:
            return ngx_http_bcookie_create_v2(r, ctx, conf);
        default:
            ctx->version = INVALID_VERSION;
            return NGX_ERROR;
    }
}


static ngx_int_t
ngx_http_bcookie_validate_v1(ngx_http_request_t *r, ngx_str_t *data)
{
    /* validate length */
    if (data->len != 24) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: expect 24 bytes but got %d: [%V]",
                      data->len, data);
        return NGX_ERROR;
    }

    /* work on copy as validation will change the value */
    u_char tmp[24];
    ngx_memcpy(tmp, data->data, 24);

    /* validate our own encoding tricks */
    if (tmp[22] != tmp[ tmp[0] % 22 ]) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: byte #22 expect %c but got %c: [%V]",
                      tmp[ tmp[0] % 22 ], tmp[22], data);
        return NGX_ERROR;
    }
    if (tmp[23] != tmp[ tmp[22] % 22 ]) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: byte #23 expect %c but got %c: [%V]",
                      tmp[ tmp[22] % 22 ], tmp[23], data);
        return NGX_ERROR;
    }
    int index;
    for (index=0; index<22; index++) {
        switch (tmp[index]) {
            case '_':
                tmp[index] = '/';
                break;
            case '~':
                tmp[index] = '+';
                break;
            default:
                break;
        }
    }

    /* validate base64 */
    ngx_str_t src, dst;
    u_char md5[4];
    src.len = 22;
    src.data = tmp;

     dst.len = 4;
     dst.data = md5;
   
    if (ngx_decode_base64(&dst, &src) == NGX_ERROR) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: [%V] is not a valid base64 string: [%V]",
                      &src, data);
        return NGX_ERROR;
    }

    return NGX_OK;
}


static ngx_int_t
ngx_http_bcookie_validate_v2(ngx_http_request_t *r, ngx_str_t *data)
{
    /* validate length */
    if (data->len != 24) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: expect 24 bytes but got %d: [%V]",
                      data->len, data);
        return NGX_ERROR;
    }

    /* work on copy as validation will change the value */
    u_char tmp[24];
    ngx_memcpy(tmp, data->data, 24);

    /* validate our own encoding tricks */
    if (tmp[22] != tmp[ tmp[0] % 22 ]) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: byte #22 expect %c but got %c: [%V]",
                      tmp[ tmp[0] % 22 ], tmp[22], data);
        return NGX_ERROR;
    }
    if (tmp[23] != tmp[ tmp[22] % 22 ]) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: byte #23 expect %c but got %c: [%V]",
                      tmp[ tmp[22] % 22 ], tmp[23], data);
        return NGX_ERROR;
    }
    int index;
    for (index=0; index<22; index++) {
        switch (tmp[index]) {
            case '_':
                tmp[index] = '/';
                break;
            case '.':
                tmp[index] = '+';
                break;
            default:
                break;
        }
    }

    /* validate base64 */
    ngx_str_t src, dst;
    u_char md5[4];
    src.len = 22;
    src.data = tmp;

     dst.len = 4;
     dst.data = md5;
   
    if (ngx_decode_base64(&dst, &src) == NGX_ERROR) {
        ngx_log_error(NGX_LOG_INFO, r->connection->log, 0,
                      "bcookie: [%V] is not a valid base64 string: [%V]",
                      &src, data);
        return NGX_ERROR;
    }

    return NGX_OK;
}
