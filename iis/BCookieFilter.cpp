#define _WIN32_WINNT 0x0400

#include <windows.h>
#include <httpfilt.h>
#include <wincrypt.h>

#include <string>
#include <sstream>

#define MD5SUM_BYTES        16  // 128 bits
#define COOKIE_DATA_LENGTH  24  // base64'd md5, plus ending \0

#define HEADER_BUFFER_SIZE      4096
#define INVALID_COOKIE_VERSION  0
#define DEFAULT_COOKIE_NAME     "B"

struct CookieContext {
	UINT16 uVersion;
	std::string strName;
	std::string strDomain;
	std::string strData;
#ifdef _DEBUG
	std::string strDebug;
#endif
} bcookie;

char EncodingTable_v1[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '~', '_'
};

char EncodingTable_v2[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '.', '_'
};

bool IsValidCookie_Algorithm1(const std::string strCookieData, char EncodingTable[])
{
	// at least needs to have right length
	if (strCookieData.size() != COOKIE_DATA_LENGTH)
	{
		return false;
	}

	// our simple checksum trick
	if ((strCookieData[22] != strCookieData[strCookieData[0] % 22]) || (strCookieData[23] != strCookieData[strCookieData[22] % 22]))
	{
		return false;
	}

	// valid cookie is composed with characters from EncodingTable
	for (UINT index=0; index<COOKIE_DATA_LENGTH; index++)
	{
		if (!isalnum(strCookieData[index]) && (strCookieData[index] != EncodingTable[62]) && (strCookieData[index] != EncodingTable[63]))
		{
			return false;
		}
	}

	return true;
}

bool IsValidCookie_v1(const std::string & strCookieData)
{
	return IsValidCookie_Algorithm1(strCookieData, EncodingTable_v1);
}

bool IsValidCookie_v2(const std::string & strCookieData)
{
	return IsValidCookie_Algorithm1(strCookieData, EncodingTable_v2);
}

void ComposeBCookie_v2(PHTTP_FILTER_CONTEXT pfc)
{
	HCRYPTPROV hProv;
	if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
	{
		// cannot initialize crypt context, thus cannot make a bcookie
		bcookie.uVersion = INVALID_COOKIE_VERSION;
		return;
	}

	HCRYPTHASH hHash;
	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		// cannot initialize crypt context, thus cannot make a bcookie
		bcookie.uVersion = INVALID_COOKIE_VERSION;
		CryptReleaseContext(hProv, 0);
		return;
	}

	// get current time, down to nano second
	FILETIME ftNow;
	GetSystemTimeAsFileTime(&ftNow);
	unsigned long long nanoNow = (((unsigned long long)ftNow.dwHighDateTime << 32) | ftNow.dwLowDateTime) / 10 - 11644473600000000ULL;

	// get server's IP and port
	char szServerIP[HEADER_BUFFER_SIZE];
	DWORD dwBufferSize = HEADER_BUFFER_SIZE;
	if (!pfc->GetServerVariable(pfc, "LOCAL_ADDR", szServerIP, &dwBufferSize))
	{
		wsprintfA(szServerIP, "ServerIP");
	}
	char szServerPort[HEADER_BUFFER_SIZE];
	dwBufferSize = HEADER_BUFFER_SIZE;
	if (!pfc->GetServerVariable(pfc, "SERVER_PORT", szServerPort, &dwBufferSize))
	{
		wsprintfA(szServerPort, "ServerPort");
	}

	// get client's IP and port
	char szClientIP[HEADER_BUFFER_SIZE];
	dwBufferSize = HEADER_BUFFER_SIZE;
	if (!pfc->GetServerVariable(pfc, "REMOTE_ADDR", szClientIP, &dwBufferSize))
	{
		wsprintfA(szClientIP, "ClientIP");
	}
	char szClientPort[HEADER_BUFFER_SIZE];
	dwBufferSize = HEADER_BUFFER_SIZE;
	if (!pfc->GetServerVariable(pfc, "REMOTE_PORT", szClientPort, &dwBufferSize))
	{
		wsprintfA(szClientPort, "ClientPort");
	}
	char szRawID[HEADER_BUFFER_SIZE];
	wsprintfA(szRawID, "%d.%d:%s:%s:%s:%s", nanoNow/1000000, nanoNow % 1000000, szServerIP, szServerPort, szClientIP, szClientPort);

	BYTE md5[MD5SUM_BYTES];
	dwBufferSize = MD5SUM_BYTES;
	if (!CryptHashData(hHash, (BYTE *)szRawID, (DWORD)strlen(szRawID), 0)
		|| !CryptGetHashParam(hHash, HP_HASHVAL, md5, &dwBufferSize, 0))
	{
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv, 0);
		bcookie.uVersion = INVALID_COOKIE_VERSION;
		return;
	}
	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);

	// base64
	UINT index = 0;
	while (index<MD5SUM_BYTES)
	{
		DWORD octet_a = index < MD5SUM_BYTES ? md5[index++] : 0;
		DWORD octet_b = index < MD5SUM_BYTES ? md5[index++] : 0;
		DWORD octet_c = index < MD5SUM_BYTES ? md5[index++] : 0;

		DWORD triple = (octet_a << 16) | (octet_b << 8) | octet_c;

		bcookie.strData.push_back(EncodingTable_v2[(triple >> 3 * 6) & 0x3F]);
		bcookie.strData.push_back(EncodingTable_v2[(triple >> 2 * 6) & 0x3F]);
		bcookie.strData.push_back(EncodingTable_v2[(triple >> 1 * 6) & 0x3F]);
		bcookie.strData.push_back(EncodingTable_v2[(triple >> 0 * 6) & 0x3F]);
	}
	bcookie.strData[22] = bcookie.strData[bcookie.strData[0] % 22];
	bcookie.strData[23] = bcookie.strData[bcookie.strData[22] % 22];

	bcookie.uVersion = 2;
	bcookie.strName = DEFAULT_COOKIE_NAME;
}

bool HasValidBCookie(PHTTP_FILTER_CONTEXT pfc, PHTTP_FILTER_PREPROC_HEADERS ph, std::string & strCookieHeader)
{
	DWORD dwBufferSize = HEADER_BUFFER_SIZE;
	char *szCookieBuffer = (char *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, dwBufferSize);
	if (!szCookieBuffer)
	{
		return false;
	}

	if (!ph->GetHeader(pfc, "Cookie:", szCookieBuffer, &dwBufferSize))
	{
		// something's wrong
		switch (GetLastError())
		{
		case ERROR_INSUFFICIENT_BUFFER:
			// buffer is not big enough
			HeapFree(GetProcessHeap(), 0, szCookieBuffer);
			szCookieBuffer = (char *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, dwBufferSize);
			if (!szCookieBuffer)
			{
				szCookieBuffer = NULL;
				return false;
			}
			if (!ph->GetHeader(pfc, "Cookie:", szCookieBuffer, &dwBufferSize))
			{
				// still have problem with larger buffer - we cannot handle this
				HeapFree(GetProcessHeap(), 0, szCookieBuffer);
				szCookieBuffer = NULL;
				return false;
			}
			break;
		case ERROR_INVALID_INDEX:
			// does not have any cookie
		case ERROR_INVALID_PARAMETER:
			// should not happen
		default:
			// should not happen
			HeapFree(GetProcessHeap(), 0, szCookieBuffer);
			return false;
		}
	}

	// now we have incoming cookies in szCookieBuffer, need to parse it and to see if B cookie is there
	std::string strCookieBuffer(szCookieBuffer);
	HeapFree(GetProcessHeap(), 0, szCookieBuffer);

	// we need to use cookie header in case bcookie from request is invalid
	strCookieHeader = strCookieBuffer;

	// find start of BCookie from cookie header
	size_t nStart = strCookieBuffer.find(bcookie.strName + "=");
	while (nStart != std::string::npos)
	{
		if ((nStart == 0) || (strCookieBuffer[nStart-1] == ' ') || (strCookieBuffer[nStart-1] == ';'))
		{
			// either found the pattern at the beginning, or just after a valid delimiter (space or ';')
			break;
		}
		nStart = strCookieBuffer.find(bcookie.strName + "=", nStart+bcookie.strName.size()+1);
	}

	if (nStart == std::string::npos)
	{
		// cannot find a valid start of BCookie
		return false;
	}
	// ignore '<BCookieName>=' part
	nStart += bcookie.strName.size() + 1;

	// find end of BCookie
	size_t nEnd = strCookieBuffer.find_first_of(" ;", nStart+1);
	if (nEnd == std::string::npos)
	{
		strCookieBuffer = strCookieBuffer.substr(nStart);
	}
	else
	{
		strCookieBuffer = strCookieBuffer.substr(nStart, nEnd - nStart);
	}

	// let's parse BCookie
	nStart = strCookieBuffer.find('&');
	if (nStart == std::string::npos)
	{
		// cannot find the delimiter between version portion and data portion
		return false;
	}

	std::string strVersion;
	std::string strData;
	if ((strCookieBuffer.substr(0, 2).compare("v=") == 0) && (strCookieBuffer.substr(nStart+1, 2).compare("d=") == 0))
	{
		// version first, then data
		strVersion = strCookieBuffer.substr(2, nStart-2);
		strData = strCookieBuffer.substr(nStart+3);
	}
	else if ((strCookieBuffer.substr(0, 2).compare("d=") == 0) && (strCookieBuffer.substr(nStart+1, 2).compare("v=") == 0))
	{
		// data first, then version
		strData = strCookieBuffer.substr(2, nStart-2);
		strVersion = strCookieBuffer.substr(nStart+3);
	}
	else
	{
		// none of above
		return false;
	}

	// validate version
	if (strVersion.find_first_not_of("0123456789") != std::string::npos)
	{
		// version contains non-digit character
		return false;
	}
	UINT version = atoi(strVersion.c_str());

	// validate data based on version
	return ((version == 1) && IsValidCookie_v1(strData)) || ((version == 2) && IsValidCookie_v2(strData));
}

void CreateBCookie(PHTTP_FILTER_CONTEXT pfc)
{
	ComposeBCookie_v2(pfc);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		// Disable the DLL_THREAD_ATTACH and DLL_THREAD_DETACH notification calls.
		// We don't require these so don't take up the overhead...
		DisableThreadLibraryCalls(hModule);
	}
	return TRUE;
}

extern "C" BOOL WINAPI GetFilterVersion(PHTTP_FILTER_VERSION pVer)
{
	// basic setup for the filter
	pVer->dwFilterVersion = HTTP_FILTER_REVISION;
	strcpy_s(pVer->lpszFilterDesc, sizeof(pVer->lpszFilterDesc), "BCookieFilter");

	// we hook up with two events:
	// PREPROC_HEADERS: will check incoming cookie header and change it in case BCookie is not presdent
	// SEND_RESPONSE: will add Set-Cookie header if original incoming cookie header does not have BCookie
	pVer->dwFlags = (SF_NOTIFY_PREPROC_HEADERS | SF_NOTIFY_SEND_RESPONSE);

	// initial value
	bcookie.uVersion = INVALID_COOKIE_VERSION;
	bcookie.strName = DEFAULT_COOKIE_NAME;
	bcookie.strDomain.clear();
	bcookie.strData.clear();
#ifdef _DEBUG
	bcookie.strDebug.clear();
#endif
	return TRUE;
}

extern "C" DWORD WINAPI HttpFilterProc(PHTTP_FILTER_CONTEXT pfc, DWORD NotificationType, LPVOID pvNotification)
{
	if (NotificationType == SF_NOTIFY_PREPROC_HEADERS)
	{
		bcookie.uVersion = INVALID_COOKIE_VERSION;
		bcookie.strData.clear();
#ifdef _DEBUG
		bcookie.strDebug.clear();
#endif	return TRUE;

		// header of incoming request is ready
		PHTTP_FILTER_PREPROC_HEADERS ph = (PHTTP_FILTER_PREPROC_HEADERS)pvNotification;
		if (pfc == NULL || ph == NULL)
		{
			SetLastError(ERROR_INVALID_PARAMETER);
			return SF_STATUS_REQ_ERROR;
		}

		std::string strCookieHeader;
		if (!HasValidBCookie(pfc, ph, strCookieHeader))
		{
			// generate new cookie if existing one is invalid
			CreateBCookie(pfc);

			// need to put to request header so that follow-up handlers take the cookie
			std::stringstream strBuffer;
			strBuffer << bcookie.strName << "=v=" << bcookie.uVersion << "&d=" << bcookie.strData << ";" << strCookieHeader;
			bcookie.strDebug = strBuffer.str();
			ph->SetHeader(pfc, "Cookie:", (LPSTR)strBuffer.str().c_str());
		}
	}
	else if (NotificationType == SF_NOTIFY_SEND_RESPONSE)
	{
		// header of outgoing response is ready
		PHTTP_FILTER_SEND_RESPONSE pr = (PHTTP_FILTER_PREPROC_HEADERS)pvNotification;
		if (pfc == NULL || pr == NULL)
		{
			SetLastError(ERROR_INVALID_PARAMETER);
			return SF_STATUS_REQ_ERROR;
		}

		if (bcookie.uVersion != INVALID_COOKIE_VERSION)
		{
			// need to issue Set-Cookie header
			std::stringstream strBuffer;
			if (bcookie.strDomain.empty())
			{
				strBuffer << bcookie.strName << "=v=" << bcookie.uVersion << "&d=" << bcookie.strData << "; path=/";
			}
			else
			{
				strBuffer << bcookie.strName << "=v=" << bcookie.uVersion << "&d=" << bcookie.strData << "; path=/; domain=" << bcookie.strDomain;
			}
			pr->AddHeader(pfc, "Set-Cookie:", (LPSTR)strBuffer.str().c_str());
#ifdef _DEBUG
			pr->AddHeader(pfc, "X-DEBUG:", (LPSTR)bcookie.strDebug.c_str());
#endif
		}
	}

	return SF_STATUS_REQ_NEXT_NOTIFICATION;
}

extern "C" BOOL WINAPI TerminateFilter(DWORD dwFlags)
{
	//	Perform the cleanup
	return TRUE;
}
